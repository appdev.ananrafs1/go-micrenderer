package scrapper

import (
	"github.com/hashicorp/go-plugin"
	"gitlab.com/appdev.ananrafs1/go-micrenderer/orchestrator"
	"gitlab.com/appdev.ananrafs1/go-micrenderer/orchestrator/shared"
	"gitlab.com/appdev.ananrafs1/go-micrenderer/utils"
	"gitlab.com/appdev.ananrafs1/go-micrenderer/model"
	"os/exec"
	"log"
	"path/filepath"
)

func ScrapAll(Host, Title string) (model.Comic, error) {
	nameFile := filepath.Join("plugins",Host)
	if exist, err := utils.IsExists(nameFile); !exist {
		return model.Comic{}, err
	}

	client := plugin.NewClient(&plugin.ClientConfig{
		HandshakeConfig: orchestrator.Handshake,
		Plugins:         orchestrator.PluginMap,
		Cmd:             exec.Command(nameFile),
		
	})
	defer client.Kill()
	
	rpcClient, err := client.Client()
	if err != nil {
		log.Fatal(err)
	}
	
	raw, err := rpcClient.Dispense("scrapper")
	if err != nil {
		log.Fatal(err)
	}
	scrap := raw.(shared.Scrapper)
	return scrap.ScrapAll(Title)
}
