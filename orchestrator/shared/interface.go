package shared
import (
	// "github.com/hashicorp/go-plugin"
	"gitlab.com/appdev.ananrafs1/go-micrenderer/model"
	// "net/rpc"
)


type Scrapper interface {
	ScrapAll(Title string) (model.Comic, error)
	ScrapPerChapter(Title, Id string) (model.Chapter, error)
}
